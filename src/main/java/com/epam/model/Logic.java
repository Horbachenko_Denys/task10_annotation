package com.epam.model;

import com.epam.model.reflection.ReflectionMethod;

import java.lang.annotation.Annotation;

public class Logic implements Model {


    @Override
    public void createAnnotation() {
        Person person = new Person("Den", "Horbacze");

        Class personClass = person.getClass();
        Annotation annotation = personClass.getAnnotation(Employee.class);
        Employee employee = (Employee) annotation;

//       We also can create method "getAnnotation" to re-use logic of getting annotations.
//                Employee employee1 = (Employee) getAnnotation(personClass, Employee.class);

        System.out.println(person.name);
        System.out.println(person.surname);
        System.out.println(employee.position());
        System.out.println(employee.company());

    }

    @Override
    public void invokeMethods() {
        ReflectionMethod.invokeThreeMethods(new ThreeMethods());
    }

    private Annotation getAnnotation(Object rootObject, Class annotationClass) {
        Class rootClass = rootObject.getClass();
        Annotation annotation = rootClass.getAnnotation(annotationClass);
        return annotation;
    }
}
