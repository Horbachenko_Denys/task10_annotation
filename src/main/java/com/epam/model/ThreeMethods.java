package com.epam.model;

public class ThreeMethods {

    public int sum(int x, int y) {
        return x + y;
    }

    private double sum(double x, double y) {
        return x + y;
    }

    public float sum(float x, float y) {
        return x + y;
    }
}
