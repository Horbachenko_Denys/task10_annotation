package com.epam.model.reflection;

import com.epam.model.ThreeMethods;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class ReflectionMethod {
    public static void invokeThreeMethods(ThreeMethods methods) {
        Class<ThreeMethods> clazz = ThreeMethods.class;
        try {
            Method intMethod = clazz.getDeclaredMethod("sum", int.class, int.class);
            System.out.println(intMethod.invoke(methods, 4, 7));
            Method floatMethod = clazz.getDeclaredMethod("sum", float.class, float.class);
            System.out.println(floatMethod.invoke(methods, 1.5F, 2.5F));
            Method doubleMethod = clazz.getDeclaredMethod("sum", double.class, double.class);
            doubleMethod.setAccessible(true);
            System.out.println(doubleMethod.invoke(methods, 2.5, 10.5));
        } catch (NoSuchMethodException |InvocationTargetException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }
}
