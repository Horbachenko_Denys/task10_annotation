package com.epam.view;

import com.epam.model.Logic;
import com.epam.model.Model;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public class MyView {

    public static Logger logger = LogManager.getLogger(MyView.class);
    private static Scanner input = new Scanner(System.in);
    private Model model;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;

    public MyView() {
        model = new Logic();
        menu = new LinkedHashMap<>();
        menu.put("1", " 1 - create annotation...");
        menu.put("2", " 2 - invoke three method...");
        menu.put("Q", " Q - Exit");

        this.methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
    }

    private void pressButton1() {
       model.createAnnotation();
    }

    private void pressButton2() {
        model.invokeMethods();
    }

    //--------------------------------------------------------------------------------------

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
                e.printStackTrace();
                logger.error("Error in menu");
            }
        } while (!keyMenu.equals("Q"));
    }
}
